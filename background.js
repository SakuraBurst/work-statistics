const reg = {
  "vk": /vk\.com/, 
  "youTube": /youtube\.com/,
  "dtf": /dtf\.ru/,
  "twitch": /twitch\.tv/
};
let timerHours = 0
    timerMinutes = 0
    timerSeconds = 0
    timerTotal = 0
    newSiteName = ''
    oldSiteName = ''
    interval = null
    currentUrl = ''

let keysArr = Object.keys(reg)

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
      if(request.get == "data"){
        setLocalStore(newSiteName)
        sendResponse({respone: true})
      }
      else if(request.get == "reg"){
        sendResponse({respone: true, data: reg})
      }
  });


function getLocalStore(a){
  let hoursStirng = a + "hours"
      minutesString = a + "minutes"
      secondsString = a + "seconds"
      totalString = a + "total";

console.log(secondsString)
  chrome.storage.local.get([hoursStirng,minutesString,secondsString,totalString], function(result) {
      console.log(result);
      if(result[totalString]){
          timerHours = result[hoursStirng]
          timerMinutes = result[minutesString]
          timerSeconds = result[secondsString]
          timerTotal = result[totalString]
      }
      else{     
          timerHours = 0
          timerMinutes = 0
          timerSeconds = 0
          timerTotal = 0

      }
  });
}

function setLocalStore(a){
  if(a){
  let hoursStirng = a + "hours"
      minutesString = a + "minutes"
      secondsString = a + "seconds"
      totalString = a + "total";
      console.log(secondsString)
  chrome.storage.local.set({[hoursStirng]: timerHours, [minutesString]: timerMinutes, [secondsString]: timerSeconds, [totalString]: timerTotal}, function() {
         console.log('Value is set to ' + timerHours, timerMinutes, timerSeconds);
       });
  }
}


function timerStartEnd(e){
  if(e == 'start' && !interval){
    interval = setInterval(timer, 1000)
  }
  if(e == 'end'){
    clearInterval(interval)
  }
}


function timer(){
     timerTotal++
     timerSeconds++
     console.log(timerSeconds)
     if(timerSeconds >= 60){
         timerSeconds = 0;
         timerMinutes++
         if(timerMinutes >= 60){
             timerMinutes = 0;
             timerHours++
         }  
     }
     
}


function onUrlUpdate(a, parametr){
  console.log(parametr)
  let trigger = false
    if(a[0].url){
      currentUrl = a[0].url
      console.log(currentUrl)
    }
    for(let i = 0; i < keysArr.length; i++){
      if(reg[keysArr[i]].test(a[0].url)){
        trigger = true
        oldSiteName = newSiteName
        newSiteName = keysArr[i]
      }
    }
    if(trigger && !interval){
      console.log('2')
      
      getLocalStore(newSiteName)
      timerStartEnd('start')
    }
    else if(interval && trigger){
      setLocalStore(oldSiteName)
      console.log('3')
      getLocalStore(newSiteName)
    }
    else if(currentUrl && interval && !trigger){
      console.log('4')
      console.log('clear')
      setLocalStore(newSiteName)
      timerStartEnd('end')
      newSiteName = ''
      oldSiteName = ''
      interval = null
      currentUrl = ''
    }
}


chrome.tabs.onUpdated.addListener(function(){
  chrome.tabs.query({active: true, currentWindow: true}, function alert(tabs1) {
    onUrlUpdate(tabs1, 'updated')
    });
})


chrome.tabs.onCreated.addListener(function(){
  chrome.tabs.query({active: true, currentWindow: true}, function alert(tabs1) {
    onUrlUpdate(tabs1, 'created')
    });
})
chrome.tabs.onActivated.addListener(function(){
  chrome.tabs.query({active: true, currentWindow: true}, function alert(tabs1) {
    onUrlUpdate(tabs1, 'activated')
    });
})

chrome.runtime.onMessage.addListener(function(request, sender ,sendResponse){
    sendResponse({data: reg})
})
 
/*

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
      console.log(sender.tab ?
                  "from a content script:" + sender.tab.url :
                  "from the extension");
      if (sender.tab.active){
        sendResponse({farewell: "true"});
      }
      if(!sender.tab.active){
        sendResponse({farewell: "false"}) 
      }
    });
*/



